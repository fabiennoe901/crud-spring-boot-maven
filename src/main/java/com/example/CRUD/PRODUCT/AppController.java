package com.example.CRUD.PRODUCT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AppController {

    @Autowired
    private productService productService;

    @RequestMapping("/")
    public String viewHomePage(Model model) {
        List<product> listproducts = productService.listAll();
        model.addAttribute("listproducts", listproducts);

        return "index";

    }


    //Show new product form
    @RequestMapping("/new")
    public String createnewproduct(Model model) {

        //new instance for product
        product newproduct = new product();

        model.addAttribute("product", newproduct);

        return "new_product";
    }


    //save new product
    @RequestMapping(value ="/save" , method= RequestMethod.POST)
    public String saveproduct(@ModelAttribute ("product") product product){
        productService.save(product);

        return "redirect:/";

    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditform(@PathVariable(name="id")Long id){
        ModelAndView mav= new ModelAndView("edit_product");

        product product = productService.get(id);
        mav.addObject("product", product);

        return mav;

    }

    //new comment



}