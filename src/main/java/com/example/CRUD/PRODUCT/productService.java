package com.example.CRUD.PRODUCT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class productService {
    @Autowired
    private ProductRepository repo;
    public List<product> listAll(){
        return repo.findAll();
    }
    //method to save product
    public void  save(product product){
        repo.save(product);
    }
    public product get(Long id){
        return repo.findById(id).get();
    }
    public void delete(Long id){
        repo.deleteById(id);
    }
}
